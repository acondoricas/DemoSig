
app.factory("DemoConfig",function(){
	
	return {
		demoUrlService:'/demo/',
	}
	
});

app.factory("SendRequest",function($http,$q,DemoConfig){
	return{
		noAuthenticatedRequest:function(_method,_url,_data){
			var deferred = $q.defer();
			var url = DemoConfig.demoUrlService + _url;
			var request = {
					method: _method,
					data: _data,
					url:url
			};
		
			$http(request)
			.success(function(response,status,headers){
				deferred.resolve(response);
			})
			.error(function(response){
				deferred.reject(response);
			})
			return  deferred.promise;
		}
	}
});